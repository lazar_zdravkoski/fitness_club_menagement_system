﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Fitness_Club_Menagement_System
{
    public partial class Form5 : Form
    {
        public Form5()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            WeightConversion.ConvertWeightsSoapClient client = new WeightConversion.ConvertWeightsSoapClient();
            var response = client.ConvertWeight(double.Parse(textBox1.Text), WeightConversion.WeightUnit.PoundsTroy, WeightConversion.WeightUnit.Kilograms);
            label4.Text = response.ToString();
        }
    }
}
