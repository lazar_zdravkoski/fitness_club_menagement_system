﻿namespace Fitness_Club_Menagement_System
{
    partial class Form4
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            this.reportViewer1 = new Microsoft.Reporting.WinForms.ReportViewer();
            this.FitnessDataSet1 = new Fitness_Club_Menagement_System.FitnessDataSet1();
            this.costumerBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.costumerTableAdapter = new Fitness_Club_Menagement_System.FitnessDataSet1TableAdapters.costumerTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.FitnessDataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.costumerBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // reportViewer1
            // 
            reportDataSource1.Name = "DataSet1";
            reportDataSource1.Value = this.costumerBindingSource;
            this.reportViewer1.LocalReport.DataSources.Add(reportDataSource1);
            this.reportViewer1.LocalReport.ReportEmbeddedResource = "Fitness_Club_Menagement_System.Report3.rdlc";
            this.reportViewer1.Location = new System.Drawing.Point(12, 12);
            this.reportViewer1.Name = "reportViewer1";
            this.reportViewer1.ServerReport.BearerToken = null;
            this.reportViewer1.Size = new System.Drawing.Size(763, 335);
            this.reportViewer1.TabIndex = 0;
            // 
            // FitnessDataSet1
            // 
            this.FitnessDataSet1.DataSetName = "FitnessDataSet1";
            this.FitnessDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // costumerBindingSource
            // 
            this.costumerBindingSource.DataMember = "costumer";
            this.costumerBindingSource.DataSource = this.FitnessDataSet1;
            // 
            // costumerTableAdapter
            // 
            this.costumerTableAdapter.ClearBeforeFill = true;
            // 
            // Form4
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(786, 359);
            this.Controls.Add(this.reportViewer1);
            this.Name = "Form4";
            this.Text = "Form4";
            this.Load += new System.EventHandler(this.Form4_Load);
            ((System.ComponentModel.ISupportInitialize)(this.FitnessDataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.costumerBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Microsoft.Reporting.WinForms.ReportViewer reportViewer1;
        private System.Windows.Forms.BindingSource costumerBindingSource;
        private FitnessDataSet1 FitnessDataSet1;
        private FitnessDataSet1TableAdapters.costumerTableAdapter costumerTableAdapter;
    }
}