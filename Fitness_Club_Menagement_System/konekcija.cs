﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Fitness_Club_Menagement_System
{
    class konekcija
    {
        public void kon(string txt1, string txt2, string cs)
        {

            MessageBox.Show("Zdravo " + txt1);

            if (txt1 == "" || txt2 == "")
            {
                MessageBox.Show("Ve molime vnesete parametri za Username i Password");
                return;
            }
            try
            {
                //Create SqlConnection
                SqlConnection con = new SqlConnection(cs);
                SqlCommand cmd = new SqlCommand("Select * from boss where UserName=@username and Password=@password", con);


                cmd.Parameters.AddWithValue("@username", txt1);
                cmd.Parameters.AddWithValue("@password", txt2);

                con.Open();
                SqlDataAdapter adapt = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                adapt.Fill(ds);
                con.Close();
                int count = ds.Tables[0].Rows.Count;
                
                if (count == 1)
                {
                    Form1 frm1 = new Form1();
                    Form3 fm = new Form3();
                    fm.Show();
                    frm1.Close();
                }
                else
                {
                    MessageBox.Show("Ve molime da vnesete validni parametri");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

    }
}
